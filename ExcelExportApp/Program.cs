﻿using ExcelExportLib;
using System;

namespace ExcelExportApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var exporter = new XlsXporter();
            exporter.ExportWithAutoSplit();
        }
    }
}
