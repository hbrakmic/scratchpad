﻿using System;
using System.Collections.Generic;
using System.IO;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;

namespace ExcelExportLib
{
    public enum ExcelEnum
    {
        MAX_CHARS = 32767
    }

    public class XlsXporter
    {
        /// <summary>
        /// Exports given sourceText to an xlsx file
        /// If no sourceText is given, the method will search for "lorem.txt" file to read from it.
        /// </summary>
        /// <param name="sourceText">Source String that should be exported to XLSX</param>
        /// <param name="sheetName">Name of the Sheet where exported string will be located</param>
        /// <param name="exportFileName">Name of the XLSX file</param>
        public void ExportWithAutoSplit(string sourceText = null, string sheetName = null, string exportFileName = null)
        {
            if (sourceText == null) // search for dummy file "lorem.txt" if no string was given
            {
                string path = @".\lorem.txt";
                if (!File.Exists(path)) // "lorem.txt" should be in the same folder where the exe file is
                {
                    throw new FileNotFoundException($"Could not find {path}");
                }
                sourceText = File.ReadAllText(path);
            }
            if (sheetName == null)
            {
                sheetName = "SheetWithExportData";
            }
            if (exportFileName == null) // if no export file name was given, use current time to generate unique file name
            {
                var date = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
                exportFileName = $"export_{date}";
            }

            using (SpreadsheetDocument xl = SpreadsheetDocument.Create($"{exportFileName}.xlsx", SpreadsheetDocumentType.Workbook))
            {
                List<OpenXmlAttribute> oxmlAttributes;
                OpenXmlWriter oxmlWriter;

                // prepare xml writer, workbook and sheet
                xl.AddWorkbookPart();
                WorksheetPart wsp = xl.WorkbookPart.AddNewPart<WorksheetPart>();

                oxmlWriter = OpenXmlWriter.Create(wsp);
                oxmlWriter.WriteStartElement(new Worksheet());
                oxmlWriter.WriteStartElement(new SheetData());

                // split strings that cross the excel limit of 32767 chars/cell
                var rounds = (sourceText.Length / Convert.ToInt32(ExcelEnum.MAX_CHARS));
                var position = 0;

                // for each string (part), create a new line and fill it with (partial) data
                for (int i = 0; i <= rounds; ++i)
                {
                    oxmlAttributes = new List<OpenXmlAttribute>
                    {
                        new OpenXmlAttribute("r", null, string.Format("{0}", i+1))
                    };

                    oxmlWriter.WriteStartElement(new Row(), oxmlAttributes);

                    oxmlAttributes = new List<OpenXmlAttribute>
                    {
                        new OpenXmlAttribute("t", null, "str")
                    };

                    oxmlWriter.WriteStartElement(new Cell(), oxmlAttributes);

                    // check, if we should take another chunk of 32767 chars or simply read until the end of string
                    var text = i < rounds ? sourceText.Substring(position, Convert.ToInt32(ExcelEnum.MAX_CHARS)) : sourceText.Substring(position);
                    oxmlWriter.WriteElement(new CellValue(text));
                    oxmlWriter.WriteEndElement();
                    oxmlWriter.WriteEndElement();
                    // move position up to 32767 chars (this is only relevant if there is "another big chunk of chars")
                    // in the case where we just read the remaining chars, the loop will be left 
                    position += Convert.ToInt32(ExcelEnum.MAX_CHARS);
                }

                // finalize the document                
                oxmlWriter.WriteEndElement();
                oxmlWriter.WriteEndElement();
                oxmlWriter.Close();

                oxmlWriter = OpenXmlWriter.Create(xl.WorkbookPart);
                oxmlWriter.WriteStartElement(new Workbook());
                oxmlWriter.WriteStartElement(new Sheets());

                oxmlWriter.WriteElement(new Sheet()
                {
                    Name = sheetName,
                    SheetId = 1,
                    Id = xl.WorkbookPart.GetIdOfPart(wsp)
                });

                oxmlWriter.WriteEndElement();
                oxmlWriter.WriteEndElement();
                oxmlWriter.Close();

                xl.Close();
            }
        }
    
    }
}
